import { render } from "@testing-library/react";
import { Router } from "react-router";
import React from "react";
import { createMemoryHistory } from "history";
import { MemoryHistory } from "history/createMemoryHistory";

export const renderWithRouter = (
  children: React.ReactNode,
  options?: {
    memoryHistory?: MemoryHistory;
    defaultPath?: string;
  }
) => {
  const { defaultPath, memoryHistory } = options ?? {};
  const history = memoryHistory ? memoryHistory : createMemoryHistory();
  if (defaultPath) {
    history.push(defaultPath);
  }
  return render(<Router history={history}>{children}</Router>);
};
