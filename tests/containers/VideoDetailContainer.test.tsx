import React from "react";
import { screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import { createMemoryHistory } from "history";
import userEvent from "@testing-library/user-event";
import { YouTubeService } from "../../src/services";
import { App } from "../../src/containers/App";
import { renderWithRouter } from "../testUtils";

const getVideosSpy = jest.spyOn(YouTubeService.prototype, "getVideos");

afterEach(() => {
  jest.clearAllMocks();
});

describe("test VideoDetailContainer (end-to-end)", function () {
  test("when user open the page, getVideos should be called", async () => {
    renderWithRouter(<App />, { defaultPath: "/video/eW7Twd85m2g" });

    expect(
      await screen.findByTestId("component-no-loading")
    ).toBeInTheDocument();
    expect(getVideosSpy).toBeCalledTimes(1);
    expect(getVideosSpy).toBeCalledWith({
      id: "eW7Twd85m2g",
      part: "snippet,contentDetails,statistics",
      regionCode: "GB",
    });
  });

  test("clicking the 'jump to home' button should redirect to /", async () => {
    const history = createMemoryHistory();

    renderWithRouter(<App />, {
      memoryHistory: history,
      defaultPath: "/video/eW7Twd85m2g",
    });

    expect(history.entries.length).toEqual(2);
    expect(history.location.pathname).toEqual("/video/eW7Twd85m2g");

    userEvent.click(await screen.findByText("← Jump to Home"));

    expect(
      await screen.findByTestId("component-no-loading")
    ).toBeInTheDocument();
    expect(history.entries.length).toEqual(3);
    expect(history.location.pathname).toEqual("/");
  });
});
