import React from "react";
import "@testing-library/jest-dom";
import { screen } from "@testing-library/react";
import { VideoCard } from "../../src/components";
import userEvent from "@testing-library/user-event";
import { renderWithRouter } from "../testUtils";
import moment from "moment";

const sampleData = {
  id: "eW7Twd85m2g",
  thumbnail: {
    url: "URL",
    width: 480,
    height: 640,
  },
  title: "The Mandalorian | Season 2 Official Trailer | Disney+",
  date: new Date("2021-11-01T14:00:22Z"),
  description: "Test Description",
};

test("clicking the play button should play the video", async () => {
  renderWithRouter(
    <VideoCard
      id={sampleData.id}
      thumbnail={sampleData.thumbnail}
      title={sampleData.title}
      date={sampleData.date}
      description={sampleData.description}
    />
  );

  userEvent.click(await screen.findByTestId("action-play-button"));

  expect(
    await screen.findByTestId("content-youtube-frame")
  ).toBeInTheDocument();
});

test("title, description, publish date should be appeared", async () => {
  renderWithRouter(
    <VideoCard
      id={sampleData.id}
      thumbnail={sampleData.thumbnail}
      title={sampleData.title}
      date={sampleData.date}
      description={sampleData.description}
    />
  );

  expect(await screen.findByText(sampleData.title)).toBeInTheDocument();
  expect(await screen.findByText(sampleData.description)).toBeInTheDocument();
  expect(
    await screen.findByText(`Published ${moment(sampleData.date).fromNow()}`)
  ).toBeInTheDocument();
});
