import React from "react";
import { useVideoDetailContainer } from "./useVideoDetailContainer";
import { VideoCard } from "../../components";
import { Link } from "react-router-dom";

export const VideoDetailContainer = () => {
  const { selectedVideo } = useVideoDetailContainer();

  if (!selectedVideo) {
    return <></>;
  }

  return (
    <div className="container" data-testid="container-video-detail">
      <div className="row">
        <div className="col-md-12 mt-5 text-center">
          <Link to="/" className="btn btn-light">
            ← Jump to Home
          </Link>
        </div>

        <div className="col-md-12 mt-5">
          <div className="d-flex flex-column align-items-center">
            <VideoCard
              id={selectedVideo.id}
              thumbnail={
                selectedVideo.snippet.thumbnails.standard ??
                selectedVideo.snippet.thumbnails.default
              }
              title={selectedVideo.snippet.title}
              description={selectedVideo.snippet.description}
              date={selectedVideo.snippet.publishedAt}
              disableViewDetail
            />
          </div>
        </div>
      </div>
    </div>
  );
};
