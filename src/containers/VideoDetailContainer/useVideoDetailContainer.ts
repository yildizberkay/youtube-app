import { useVideoList } from "../../hooks";
import { useEffect } from "react";
import { useParams } from "react-router-dom";

export const useVideoDetailContainer = () => {
  const { videos, loadVideos } = useVideoList();
  const { videoId } = useParams<{ videoId: string }>();

  useEffect(() => {
    loadVideos({ id: videoId });
  }, [videoId]);

  const selectedVideo = videos[0];

  return { selectedVideo };
};
