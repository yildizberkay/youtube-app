import { useEffect, useMemo } from "react";
import { useCategoryList, useQuery, useVideoList } from "../../hooks";
import { useParams } from "react-router-dom";

const highlightedCategoryIds = ["10", "17", "20", "15", "1"];

export const useVideoListContainer = () => {
  const { videos, loadVideos, pageTokens, hasNextPage, hasPrevPage } =
    useVideoList();
  const { categories, loadCategories } = useCategoryList();
  const { categoryId: videoCategoryId = null } =
    useParams<{ categoryId: string }>();
  const urlQuery = useQuery();
  const pageToken = urlQuery.get("pageToken");

  useEffect(() => {
    loadCategories();
  }, []);

  useEffect(() => {
    loadVideos({ videoCategoryId, pageToken, regionCode: "GB" });
  }, [videoCategoryId, pageToken]);

  const highlightedCategories = useMemo(
    () =>
      categories.filter((category) =>
        highlightedCategoryIds.includes(category.id)
      ),
    [categories]
  );

  const prevURL = `/${videoCategoryId ?? ""}${
    hasPrevPage && "?pageToken=" + pageTokens.prevPageToken
  }`;
  const nextURL = `/${videoCategoryId ?? ""}${
    hasNextPage && "?pageToken=" + pageTokens.nextPageToken
  }`;

  return {
    videos,
    highlightedCategories,
    nextURL,
    prevURL,
    hasNextPage,
    hasPrevPage,
    videoCategoryId,
  };
};
