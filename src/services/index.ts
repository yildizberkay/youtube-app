import { NetworkClient } from "../libs/NetworkClient";
import { YouTubeService } from "./YouTubeService";
export { YouTubeService } from "./YouTubeService";

const youtubeClient = new NetworkClient(
  "https://www.googleapis.com/youtube/v3"
);

youtubeClient.getNetworkInstance().interceptors.request.use((config) => {
  config.params = {
    ...config.params,
    key: process.env.YOUTUBE_API_KEY,
  };
  return config;
});

export const youTubeService = new YouTubeService(youtubeClient);
