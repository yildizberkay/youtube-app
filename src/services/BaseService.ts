import { INetworkClient } from "../interfaces";

export class BaseService {
  protected networkClient: INetworkClient;

  constructor(networkClient: INetworkClient) {
    this.networkClient = networkClient;
  }
}
