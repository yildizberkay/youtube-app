import { BaseService } from "./BaseService";
import { VideoListResponse } from "../types/responses";
import { VideoCategoryListResponse } from "../types/responses";
import { GetVideoCategoriesParams, GetVideosParams } from "../types/services";

export class YouTubeService extends BaseService {
  async getVideos(params: GetVideosParams) {
    return this.networkClient
      .getNetworkInstance()
      .get<VideoListResponse>("/videos", { params });
  }

  async getVideoCategories(params: GetVideoCategoriesParams) {
    return this.networkClient
      .getNetworkInstance()
      .get<VideoCategoryListResponse>("/videoCategories", { params });
  }
}
