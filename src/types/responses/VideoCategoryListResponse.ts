import { VideoCategoryItem } from "../youtube";

export interface VideoCategoryListResponse {
  kind: string;
  etag: string;
  items: VideoCategoryItem[];
}
