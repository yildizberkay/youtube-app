import { VideoItem } from "../youtube";
import { PageInfo } from "../youtube";

export interface VideoListResponse {
  kind: string;
  etag: string;
  items: VideoItem[];
  nextPageToken?: string;
  prevPageToken?: string;
  pageInfo: PageInfo;
}
