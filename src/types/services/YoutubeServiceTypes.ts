export type GetVideosParams = {
  id?: string;
  part: string;
  chart?: "mostPopular";
  maxResults?: number;
  pageToken?: string;
  videoCategoryId?: string;
  regionCode?: string;
};

export type GetVideoCategoriesParams = { part: string; regionCode: string };
