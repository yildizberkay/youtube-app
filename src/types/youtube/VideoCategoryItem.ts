import { VideoCategorySnippet } from "./VideoCategorySnippet";

export interface VideoCategoryItem {
  kind: string;
  etag: string;
  id: string;
  snippet: VideoCategorySnippet;
}
