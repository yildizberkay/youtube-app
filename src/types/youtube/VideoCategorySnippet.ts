export interface VideoCategorySnippet {
  title: string;
  assignable: boolean;
  channelId: string;
}
