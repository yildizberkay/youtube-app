import React, { memo, useState } from "react";
import moment from "moment";
import { playImage } from "../assets/images";
import { Thumbnail } from "../types/youtube";
import { Link } from "react-router-dom";

export const VideoCard = memo(
  (props: {
    id: string;
    thumbnail: Thumbnail;
    title: string;
    description?: string;
    date: Date;
    disableViewDetail?: boolean;
  }) => {
    const { id, thumbnail, title, description, date, disableViewDetail } =
      props;
    const [playState, setPlayState] = useState(false);

    return (
      <div className="video-card card mb-3 fade-in">
        {playState ? (
          <div className="youtube-frame">
            <iframe
              data-testid="content-youtube-frame"
              width="640"
              height="480"
              src={`https://www.youtube.com/embed/${id}?mute=1&autoplay=1`}
              frameBorder="0"
            />
          </div>
        ) : (
          <>
            <div className="play-content">
              <img
                data-testid="action-play-button"
                onClick={() => setPlayState(true)}
                src={playImage}
                alt="Play the video"
              />
            </div>
            <img src={thumbnail.url} className="card-img-top" alt={title} />
          </>
        )}

        <div className="card-body">
          <h5 className="card-title">{title}</h5>
          <h6 className="card-subtitle mb-2 text-muted">
            Published {moment(date).fromNow()}
          </h6>
          {description && (
            <p style={{ whiteSpace: "pre-wrap" }} className="card-text">
              {description}
            </p>
          )}
          {!disableViewDetail && (
            <Link to={`/video/${id}`} className="card-link">
              View Details
            </Link>
          )}

          <a
            href={`https://www.youtube.com/watch?v=${id}`}
            target="_blank"
            className="card-link"
          >
            View on YouTube
          </a>
        </div>
      </div>
    );
  }
);
