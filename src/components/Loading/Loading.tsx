import React, { useCallback, useEffect, useState } from "react";
import { loadingEvent } from "./loadingEvent";
import debounce from "lodash.debounce";

export const Loading = () => {
  const [loadingState, setLoadingState] = useState<boolean>(false);

  const onShowLoading = useCallback(
    debounce(
      () => {
        setLoadingState(true);
      },
      300,
      { leading: true, trailing: false }
    ),
    []
  );
  const onHideLoading = useCallback(
    debounce(
      () => {
        setLoadingState(false);
      },
      300,
      { leading: false, trailing: true }
    ),
    []
  );

  useEffect(() => {
    loadingEvent.addEventListener("onShowLoading", onShowLoading);
    loadingEvent.addEventListener("onHideLoading", onHideLoading);

    // Cleanup!
    return () => {
      loadingEvent.removeEventListener("onShowLoading", onShowLoading);
      loadingEvent.removeEventListener("onHideLoading", onHideLoading);
    };
  }, []);

  if (loadingState) {
    return (
      <div className="loading-container d-flex justify-content-center align-items-center">
        <div>
          <div className="d-flex flex-row align-items-center">
            <div
              className="spinner-border"
              style={{ width: 36, height: 36 }}
              role="status"
            >
              <span className="visually-hidden">Loading...</span>
            </div>
            <div style={{ marginLeft: 12 }}>
              <b>Loading...</b>
            </div>
          </div>
        </div>
      </div>
    );
  }

  return <div data-testid="component-no-loading" />;
};
