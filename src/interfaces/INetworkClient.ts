import { AxiosInstance } from "axios";

export interface INetworkClient {
  readonly networkInstance: AxiosInstance;
  getNetworkInstance(): AxiosInstance;
}
