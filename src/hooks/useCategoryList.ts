import { useState } from "react";
import { VideoCategoryItem } from "../types/youtube";
import { youTubeService } from "../services";
import { useLoading } from "../components";

export const useCategoryList = () => {
  const [categories, setCategories] = useState<VideoCategoryItem[]>([]);
  const { showLoading, hideLoading } = useLoading();
  const loadCategories = () => {
    showLoading();
    youTubeService
      .getVideoCategories({ part: "snippet", regionCode: "GB" })
      .then((response) => {
        setCategories(response.data.items);
      })
      .finally(() => hideLoading());
  };

  return { categories, loadCategories };
};
