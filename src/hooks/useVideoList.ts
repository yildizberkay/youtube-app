import { useState } from "react";
import { VideoItem } from "../types/youtube";
import { youTubeService } from "../services";
import { useLoading } from "../components";
import { GetVideosParams } from "../types/services";

export const useVideoList = () => {
  const [videos, setVideos] = useState<VideoItem[]>([]);
  const [pageTokens, setPageTokens] = useState<{
    nextPageToken?: string;
    prevPageToken?: string;
  }>({});
  const { showLoading, hideLoading } = useLoading();

  const loadVideos = async (params: Partial<GetVideosParams>) => {
    const {
      id,
      part = "snippet,contentDetails,statistics",
      chart = "mostPopular",
      maxResults = 10,
      pageToken,
      videoCategoryId,
      regionCode = "GB",
    } = params;

    const getVideosParams = id
      ? { id, part, regionCode }
      : {
          part,
          chart,
          maxResults,
          pageToken,
          videoCategoryId,
          regionCode,
        };

    showLoading();
    return youTubeService
      .getVideos(getVideosParams)
      .then((response) => {
        setVideos(response.data.items);
        setPageTokens({
          nextPageToken: response.data.nextPageToken,
          prevPageToken: response.data.prevPageToken,
        });
      })
      .finally(() => hideLoading());
  };

  const hasNextPage = !!pageTokens.nextPageToken;
  const hasPrevPage = !!pageTokens.prevPageToken;

  return {
    videos,
    loadVideos,
    pageTokens,
    hasNextPage,
    hasPrevPage,
  };
};
