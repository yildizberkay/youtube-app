import axios, { AxiosInstance } from "axios";
import { INetworkClient } from "../interfaces";

export class NetworkClient implements INetworkClient {
  readonly networkInstance!: AxiosInstance;

  constructor(baseURL: string) {
    this.networkInstance = axios.create({ baseURL });
  }

  getNetworkInstance(): AxiosInstance {
    return this.networkInstance;
  }
}
