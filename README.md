# YouTube Client App
Demo: https://sample-youtube-app.surge.sh/

## Get Started

### System Requirements

|Package|Version|
|---|---|
|Docker|19.03 +|
|Docker Engine|1.40 +|
|NodeJS|14.x +|
|yarn|1.x|

### Setup
The project uses yarn@1.x as a package manager. First, if there is no yarn on your system, it should be installed.

```
npm install --global yarn
```

**Installing Dependencies**

Run the following command in the main directory of the project.
```
yarn install
```

### Environment Variable
The project needs a valid YouTube API key. Copy the ".env.sample" file as ".env" and put your API key to this file.

### Run
Run the following command and open [http://localhost:9000](http://localhost:9000).

```
yarn start
```

### Build
There are two build options as Docker and local.

The following command runs tests and builds the source in a proper environment (node:14-alpine) provided by Docker. It is the most secure way because it doesn't affect by the local environment. Because of that, its output is suitable for the production environment.

```
yarn build
```

Another option is building on the local machine. The following command will just build the source code.

```
yarn buildWithWebpack
```

### Tests

```
yarn test
```

## Architecture
The project was designed based on the "Clean Architecture". According to this architecture, inner layers don't know anything about outer layers, and it brings a loosely-coupled implementation.

**Services** represent our communication layer. API calls occur in this layer, and it knows only types.

**Hooks** carry our business logic or data that returns from services. Its name comes from React's Hooks. It can be named as use-cases as well.

**Components** are the most minimal UI elements. They don't store business logic. If they must carry a business logic, they may have their hooks.

**Containers** are the main entry points that combine components and hooks. They generally represent pages. However, a container can contain another container if necessary.

![The Architecture of YouTube App](./res/images/architecture.png)
